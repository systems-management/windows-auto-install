##############################################################################
##############################################################################
##
##  Fichier de démonstration d'installation de config
##
##  Author : Didier Bröska <didier.broska@gmail.com>
##  Description : 
##      Fichier de démonstration d'une configuration à installer
##
##############################################################################
##############################################################################

# Install and Import WindowsConfig and Boxstarter
. { iwr -useb https://gitlab.com/systems-management/windows-auto-install/raw/master/bootstrap.ps1 } | 
    iex; Install-Config -Branch "dev-branch" -Force

Import-Boxstarter "BoxStarter.Common"
Import-Boxstarter "Boxstarter.WinConfig"

Disable-UAC

RunCookbook "FileExplorerSettings"
RunCookbook "RemoveDefaultsApps"
RunCookbook "InstallDefaultsApps"

$ComputerName = Read-Host "Veuillez entrer le nom de votre ordinateur "
Rename-Computer -NewName $ComputerName -Force # Ici doit avoir lieu un reboot

RunCookbook "InstallWSLUbuntu1804"

