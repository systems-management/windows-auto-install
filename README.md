# Windows Auto Install

Ce projet est un provisionneur pour les Windows sortie de boite.

Il permet depuis un script Powershell en tant que simple Gist sur un repo de créer une configuration d'installation. Des morceaux d'installation sont déjà prévu via des recettes pré-établies (Cookbook).

Le projet est basé sur [Boxstarter](boxstarter.org) et [Chocolatey](chocolatey.org).

## Fonctionnement

Pour la créer d'un fichier de configuration, nous devons installer en local les dépendances, tel que Boxstarter, Chocolatey et les Cookbooks.

Il faut donc intégré la ligne suivante en haut du script :

```powershell
. { iwr -useb https://gitlab.com/systems-management/windows-auto-install/raw/master/bootstrap.ps1 } | 
    iex; Install-Config -Branch -Force
```

Pour l'utilisation des fonctions liées à Boxstarter, nous pouvons importer des modules de celui-ci : `Import-Boxstarter "<nom-du-module>"`.

Enfin il ne reste plus que sélectionner les recettes à vouloir installer : `RunCoobbook "<nom-du-cookbook-sans-extension>"`

## Installation d'une configuration

Pour lancer une installation, il suffit de passer la police d'exécution en ByPass pour le process demandé et d'exécuter le fichier de configuration :

```powershell
# Exemple avec le fichier de configuration de démo
Set-ExecutionPolicy Bypass -Scope Process -Force; . { iwr -useb https://gitlab.com/systems-management/windows-auto-install/raw/dev-branch/configs/template-config.ps1 } | iex;
```
