##############################################################################
##############################################################################
##
##      Install My Defaults Apps Cookbook
##
##      Author  :   Didier Bröska   <didier.broska@gmail.com
##
##############################################################################
##############################################################################

# Install my default apps

## Bitwarden
cinst -y bitwarden
Write-Host "Wait to configure Bitwarden Application"
Start-Process -FilePath "C:\Program Files\Bitwarden\Bitwarden.exe"
while ((Get-Process -Name "*bitwarden*") -like "*bitwarden*"){ }

## Firefox
cinst -y firefox-dev --pre
Write-Host "Wait to configure Firefox Application"
start-process -FilePath "C:\Program Files\Firefox Developer Edition\firefox.exe"
while ((Get-Process -Name "*firefox*") -like "*firefox*"){ }

## Thunderbird
cinst -y thunderbird
Write-Host "Wait to configure Thunderbird Application"
start-process -FilePath "C:\Program Files\Mozilla Thunderbird\thunderbird.exe"
while ((Get-Process -Name "*thunderbird*") -like "*thunderbird*"){ }

## Sysinternal
cinst -y sysinternals