##############################################################################
##############################################################################
##
##      Remove Defaults Apps Windows
##
##      Author  :   Didier Bröska   <didier.broska@gmail.com
##
##############################################################################
##############################################################################

# Remove default apps
Write-Host "Uninstall some applications that come with Windows out of the box" -ForegroundColor "Yellow"

#Referenced to build script
# https://docs.microsoft.com/en-us/windows/application-management/remove-provisioned-apps-during-update
# https://github.com/jayharris/dotfiles-windows/blob/master/windows.ps1#L157
# https://gist.github.com/jessfraz/7c319b046daa101a4aaef937a20ff41f
# https://gist.github.com/alirobe/7f3b34ad89a159e6daa1
# https://github.com/W4RH4WK/Debloat-Windows-10/blob/master/scripts/remove-default-apps.ps1
# https://github.com/microsoft/windows-dev-box-setup-scripts/blob/master/scripts/RemoveDefaultApps.ps1


function removeApp {
	Param ([string]$appName)
	Write-Host "Trying to remove $appName"
	Get-AppxPackage $appName -AllUsers | Remove-AppxPackage
    Get-AppXProvisionedPackage -Online | 
        Where-Object DisplayName -like $appName | 
        Remove-AppxProvisionedPackage -Online
}

$applicationList = @(
    "*BingFinance*"
    "*BingFinance*"
    "*BingNews*"
    "*BingSports*"
    "*BingWeather*"
    "*CommsPhone*"
    "*Getstarted*"
    "*WindowsMaps*"
    "*MarchofEmpires*"
    "*Messaging*"
    "*Minecraft*"
    "*Office*"
    "*OneConnect*"
    "*Solitaire*"
    "*XboxApp*"
    "*XboxIdentityProvider*"
    "*NetworkSpeedTest*"
    "*Autodesk*"
    "*BubbleWitch*"
    "king.com*"
    "G5*"
    "*Dell*"
    "*Facebook*"
    "*Keeper*"
    "*Netflix*"
    "*Twitter*"
    "*Plex*"
    "*.Duolingo-LearnLanguagesforFree"
    "*.EclipseManager"
    "ActiproSoftwareLLC.562882FEEB491" # Code Writer
    "*.AdobePhotoshopExpress"
);

foreach ($app in $applicationList) {
    removeApp $app
}