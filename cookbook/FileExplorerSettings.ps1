##############################################################################
##############################################################################
##
##      FileExplorer Settings Cookboox
##
##      Author  :   Didier Bröska   <didier.broska@gmail.com
##
##############################################################################
##############################################################################

# FileExplorerSettings
#--- Windows Features ---
# Show hidden, files, show protected Os files and show file extensions
Set-WindowsExplorerOptions  -EnableShowHiddenFilesFoldersDrives `
                            -EnableShowProtectedOSFiles `
                            -EnableShowFileExtensions `
                            -DisableShowRibbon `

## TODO configure pavé tactile

#--- File Explorer Settings ---
# will expand explorer to the actual folder you're in
Set-ItemProperty -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced -Name NavPaneExpandToCurrentFolder -Value 1
#adds things back in your left pane like recycle bin
Set-ItemProperty -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced -Name NavPaneShowAllFolders -Value 1
#opens PC to This PC, not quick access
Set-ItemProperty -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced -Name LaunchTo -Value 1
#taskbar where window is open for multi-monitor
Set-ItemProperty -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced -Name MMTaskbarMode -Value 2

# Onedrive Uninstall
#Kill task or process
taskkill /f /im OneDrive.exe

#Uninstall
& "$env:SystemRoot\SysWOW64\OneDriveSetup.exe" /uninstall

#TODO a refaire car ça me créer un doublon je pense
# #   Clean register
# $path = "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer"
# $shell_folder = "$path\Shell Folders\"
# $user_shell_folder = "$path\User Shell Folders\"

#Set-ItemProperty $shell_folder -Name "My Pictures" -Value "$env:USERPROFILE\Images"
#Set-ItemProperty $shell_folder -Name "Desktop" -Value "$env:USERPROFILE\Bureau"
#Set-ItemProperty $user_shell_folder -Name "My Pictures" -Value "$env:USERPROFILE\Images"
#Set-ItemProperty $user_shell_folder -Name "Desktop" -Value "$env:USERPROFILE\Bureau"
#Set-ItemProperty $user_shell_folder -Name "Personal" -Value "$env:USERPROFILE\Documents"
#Set-ItemProperty $user_shell_folder -Name "{339719B5-8C47-4894-94C2-D8F77ADD44A6}"  -Value "$env:USERPROFILE\Images"
#Set-ItemProperty $user_shell_folder -Name "{F42EE2D3-909F-4907-8871-4C22FC0BF756}" -Value "$env:USERPROFILE\Documents"
#Set-ItemProperty $user_shell_folder -Name "{0DDD015D-B06C-45D5-8C4C-F59713854639}" -Value "$env:USERPROFILE\Images"
#Set-ItemProperty $user_shell_folder -Name "{767E6811-49CB-4273-87C2-20F355E1085B}" -Value "$env:USERPROFILE\Images\Pellicule"

##   Replace and clean folder
#cp $env:USERPROFILE\OneDrive\Documents\* $env:USERPROFILE\Documents\
