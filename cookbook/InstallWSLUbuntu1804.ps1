##############################################################################
##############################################################################
##
##      CookBook - Install WSL and Ubuntu 18.04 LTS
##
##      Author  :   Didier Bröska   <didier.broska@gmail.com
##
##############################################################################
##############################################################################

# Activate wsl with ubvuntu 1804 distro and install configure
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux
Invoke-WebRequest -Uri https://aka.ms/wsl-ubuntu-1804 -OutFile Ubuntu.appx -UseBasicParsing
Add-AppxPackage .\Ubuntu.appx
Remove-Item .\Ubuntu.appx
ubuntu1804.exe install

### Disable password in sudo
ubuntu1804.exe run "sudo sed -i 's/%sudo   ALL=(ALL:ALL) ALL/%sudo   ALL=(ALL:ALL) NOPASSWD: ALL/g'"
ubuntu1804.exe run "sudo apt update;sudo apt -y upgrade"
