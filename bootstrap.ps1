##############################################################################
##############################################################################
##
##  Bootstrap script - Windows Auto Install
##
##  Author : Didier Bröska <didier.broska@gmail.com>
##  Description : 
##      TODO description
##
##############################################################################
##############################################################################

function Init-Var {
    if (!$Global:Config) {
        $Global:Config = @{}
    }
    $Global:Config.BoxstarterPath = "${env:ProgramData}\Boxstarter"
    $Global:Config.WCPath = "${env:ProgramData}\WindowsConfig"
    $Global:Config.CookbookPath = "${env:ProgramData}\WindowsConfig\cookbook"
}

function RunCookbook ([string]$cookbook) {
    ."${Global:Config.CookbookPath}\${cookbook}.ps1"
}

function Import-Boxstarter([string]$module) {
    Import-Module "${Global:Config.BoxstarterPath}\${module}\${module}.psm1"
}

# --- Install dependencies ---
function Install-Config {
    param(
        [switch]$Force,
        [string]$Branch
    )
    Init-Var
    # Boxstarter Install
    Install-Boxstarter -Force:$Force

    # Cookbook Install
    Install-Cookbook -Force:$Force -Branch:$Branch
}

function Install-Boxstarter {
    param (
        [switch]$Force
    )
    $BoxstarterInstall = Test-Path -Path $Global:Config.BoxstarterPath
    if (!$BoxstarterInstall -or $Force) {
        #FIXME force not run here
        . { iwr -useb https://boxstarter.org/bootstrapper.ps1 } | 
            iex; Get-Boxstarter -Force:$Force
    }
}

function Install-Cookbook {
    param (
        [switch]$Force,
        [string]$Branch
    )
    $CookbookInstall = Test-Path -Path $Global:Config.WCPath
    if (!$CookbookInstall -or $Force) {
        Write-Host "Windows Config Installation" -ForegroundColor Green
        
        if (!$Branch) { $Branch = master }
        if ($Force) { 
            Remove-Item -Recurse $Global:Config.WCPath -Force -ErrorAction SilentlyContinue
        }
        
        $file = "windows-auto-install-${Branch}"
        iwr -useb -OutFile "${file}.zip" -Uri "https://gitlab.com/systems-management/windows-auto-install/-/archive/${Branch}/${file}.zip"
        Expand-Archive -Path "${file}.zip" -DestinationPath $Config.WCPath
        Remove-Item -Path ".\${file}.zip"
        Copy-Item -Path "${Global:Config.WCPath}\${file}\*" -Recurse -Destination $Global:Config.WCPath
        Remove-Item -Path "${Global:Config.WCPath}\${file}" -Recurse        
    }
}

#TODO: Reboot resilience